<?php

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Schema;
use Kisphp\Db\KisphpDbal;

require_once __DIR__ . '/vendor/autoload.php';

abstract class AbstractConnection
{
    protected $params = [];

    public function createConnection()
    {
        $con = DriverManager::getConnection($this->params, new Configuration());

        return new KisphpDbal($con);
    }
}

class MysqlConnection extends AbstractConnection
{
    protected $params = [
        "dbname" => "kisphp",
        "user" => "root",
        "password" => "kisphp",
        "host" => "127.0.0.1",
        "port" => "33306",
        "driver" => "pdo_mysql",
    ];
}

class PostgresConnection extends AbstractConnection
{
    protected $params = [
        "dbname" => "kisphp",
        "user" => "postgres",
        "password" => "kisphp",
        "host" => "127.0.0.1",
        "port" => "35432",
        "driver" => "pdo_pgsql",
    ];
}

class LocalTests
{
    protected $connectors = [];

    protected $resulsts = [];

    public function addConnector(AbstractConnection $dbal)
    {
        $this->connectors[get_class($dbal)] = $dbal->createConnection();
    }

    public function showResults()
    {
        dump($this->resulsts);die;
    }

    public function runTests()
    {
        foreach ($this->connectors as $name => $con) {
//            $this->getConnectionStatus($name, $con);
            $this->getTablesList($name, $con);
            $this->getTableRows($name, $con);
        }
    }

    protected function getConnectionStatus($name, KisphpDbal $con)
    {
        $this->resulsts[$name]['status'] = $con->getStatus();
    }

    protected function getTableRows($name, KisphpDbal $con)
    {
        $this->resulsts[$name]['table_content'] = $con->getRow("SELECT id, column_1 FROM test_table LIMIT 1");
    }

    protected function getTablesList($name, KisphpDbal $db)
    {
        if ($name === MysqlConnection::class) {
            $this->resulsts[$name]['tables_list'] = $db->getRow("SHOW TABLES");
        }
        if ($name === PostgresConnection::class) {
            $this->resulsts[$name]['tables_list'] = $db->getRow("SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND 
    schemaname != 'information_schema';");
        }
    }
}

//$mysql8 = new MysqlConnection();
//$pgsql = new PostgresConnection();
//
//$lt = new LocalTests();
//$lt->addConnector($mysql8);
//$lt->addConnector($pgsql);
//
//$lt->runTests();
//
//$lt->showResults();

$sm = new Schema();

$table = $sm->createTable('test_table');
$table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
$table->addColumn('column_1', 'string', ['length' => 64]);
$table->addColumn('column_2', 'string', ['length' => 64]);
$table->addColumn('column_3', 'string', ['length' => 64]);
$table->setPrimaryKey(['id']);

$queries = $sm->toSql(new \Doctrine\DBAL\Platforms\MySQL80Platform());
$delq = $sm->toDropSql(new \Doctrine\DBAL\Platforms\MySQLPlatform());


dump($queries);
dump($delq);die;
