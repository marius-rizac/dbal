.PHONY: test test_mysql test_pgsql fix

test: test_mysql test_pgsql

define get_param
	return cat tests/config.json | jq -r ".$(1)"
endef

test_mysql:
	cp tests/config-mysql.json tests/config.json
	vendor/bin/phpunit

test_pgsql:
	cp tests/config-pgsql.json tests/config.json
	vendor/bin/phpunit


fix:
	vendor/bin/php-cs-fixer fix -v
