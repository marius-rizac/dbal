DROP TABLE IF EXISTS "test_table";
DROP SEQUENCE IF EXISTS test_table_id_seq;
CREATE SEQUENCE test_table_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."test_table" (
   "id" integer DEFAULT nextval('test_table_id_seq') NOT NULL,
   "column_1" character varying(50) NOT NULL,
   "column_2" character varying(50) NOT NULL,
   "column_3" character varying(50) NOT NULL
) WITH (oids = false);

INSERT INTO "test_table" ("column_1", "column_2", "column_3") VALUES
('c1.1',	'c1.2',	'c1.3'),
('c2.1',	'c2.2',	'c2.3'),
('c3.1',	'c3.2',	'c3.3');
