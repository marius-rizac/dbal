<?php

namespace Test\Kisphp;

use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Schema\Schema;
use Kisphp\Db\KisphpDbal;
use PHPUnit\Framework\TestCase;

class KisphpDbalTest extends TestCase
{
    /**
     * @var KisphpDbal
     */
    protected $db;

    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * @var array
     */
    protected $sqlSetup = [];

    /**
     * @var array
     */
    protected $sqlDrop = [];

    protected function setUp(): void
    {
        $con = Connection::create();

        $this->db = new KisphpDbal($con);

        $params = $con->getParams();

        $this->createPdoConnection($params);

        $this->createSchema($params);

        foreach ($this->sqlSetup as $query) {
            $this->pdo->query($query)->execute();
        }

        $this->populateTable();

        parent::setUp();
    }

    protected function populateTable()
    {
        $this->pdo->query("INSERT INTO test_table (column_1, column_2, column_3) VALUES ('c1.1', 'c1.2', 'c1.3')");
        $this->pdo->query("INSERT INTO test_table (column_1, column_2, column_3) VALUES ('c2.1', 'c2.2', 'c2.3')");
        $this->pdo->query("INSERT INTO test_table (column_1, column_2, column_3) VALUES ('c3.1', 'c3.2', 'c3.3')");
    }

    /**
     * @param array $params
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    protected function createSchema(array $params)
    {
        $sm = new Schema();

        $table = $sm->createTable('test_table');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('column_1', 'string', ['length' => 64, 'default' => '']);
        $table->addColumn('column_2', 'string', ['length' => 64, 'default' => '']);
        $table->addColumn('column_3', 'string', ['length' => 64, 'default' => '']);
        $table->setPrimaryKey(['id']);

        $this->sqlSetup = $sm->toSql($this->getPlatform($params));
        $this->sqlDrop = $sm->toDropSql($this->getPlatform($params));
    }

    /**
     * @param array $params
     * @return MySQLPlatform|PostgreSQL100Platform
     * @throws \Exception
     */
    protected function getPlatform(array $params)
    {
        if ($params['driver'] === 'pdo_mysql') {
            return new MySQLPlatform();
        }
        if ($params['driver'] === 'pdo_pgsql') {
            return new PostgreSQL100Platform();
        }

        throw new \Exception("Platform not found");
    }

    /**
     * @param array $conParams
     */
    protected function createPdoConnection(array $conParams)
    {
        $dsn = sprintf(
            '%s:dbname=%s;host=%s;port=%d',
            str_replace('pdo_', '', $conParams['driver']),
            $conParams['dbname'],
            $conParams['host'],
            (int) $conParams['port']
        );
        $this->pdo = new \PDO($dsn, $conParams['user'], $conParams['password']);
    }

    protected function tearDown(): void
    {
        foreach ($this->sqlDrop as $query) {
            $this->pdo->query($query)->execute();
        }

        parent::tearDown();
    }

    // Start testing
    public function test_connection()
    {
        $this->assertInstanceOf(\Doctrine\DBAL\Connection::class, $this->db->getConnection());
    }


    public function test_insert()
    {
        $insertedId = $this->db->insert('test_table', [
            'column_1' => 'value_1',
            'column_2' => 'value_2',
        ]);

        $this->assertGreaterThan(0, $insertedId);

        $record = $this->pdo
            ->query("SELECT * FROM test_table WHERE id = " . (int) $insertedId . " LIMIT 1")
            ->fetch(\PDO::FETCH_ASSOC);

        $this->assertSame('value_1', $record['column_1']);
        $this->assertSame('value_2', $record['column_2']);
        $this->assertSame('', $record['column_3']);

        $this->assertGreaterThan(0, $record['id']);
    }

    public function test_update_simple_value()
    {
        $affectedRows = $this->db->update('test_table', [
            'column_1' => 'value_1',
            'column_2' => 'value_2',
        ], [
            'id' => 1,
        ]);

        $this->assertEquals(1, $affectedRows);
    }

    public function test_update_simple_value_on_other_column()
    {
        $affectedRows = $this->db->update('test_table', [
            'column_1' => 'value_1',
            'column_2' => 'value_2',
        ], [
            'column_3' => 'c3.3',
        ]);

        $this->assertEquals(1, $affectedRows);
    }

    public function test_update_simple_value_on_multiple_columns()
    {
        $affectedRows = $this->db->update('test_table', [
            'column_1' => 'value_1.1',
            'column_2' => 'value_2.2',
        ], [
            'column_2' => 'c2.2',
            'column_3' => 'c2.3',
        ]);

        $this->assertEquals(1, $affectedRows);
    }

    public function test_get_value()
    {
        $value = $this->db->getValue("SELECT column_1 FROM test_table");

        $this->assertSame('c1.1', $value);
    }

    public function test_get_pairs()
    {
        $pairs = $this->db->getPairs("SELECT id, column_1 FROM test_table");

        $this->assertSame([
            1 => 'c1.1',
            2 => 'c2.1',
            3 => 'c3.1',
        ], $pairs);
    }

    public function test_getPairs_with_conditions()
    {
        $pairs = $this->db->getPairs("SELECT id, column_2 FROM test_table WHERE id > :id", [
            'id' => 0,
        ]);

        $this->assertSame([
            1 => 'c1.2',
            2 => 'c2.2',
            3 => 'c3.2',
        ], $pairs);
    }

    public function test_select()
    {
        $result = $this->db->query("SELECT * FROM test_table ");

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $this->assertGreaterThan(2, count($row));
        }
    }

    public function test_getRow()
    {
        $result = $this->db->getRow('SELECT * FROM test_table WHERE column_1 = :column', [
            'column' => 'c1.1'
        ]);

        $this->assertEquals(1, $result['id']);
        $this->assertSame('c1.1', $result['column_1']);
        $this->assertSame('c1.2', $result['column_2']);
        $this->assertSame('c1.3', $result['column_3']);
    }
}
