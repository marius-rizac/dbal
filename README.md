# Simple MySQL Database Access Layer

[![pipeline status](https://gitlab.com/kisphp/dbal/badges/master/pipeline.svg)](https://gitlab.com/kisphp/dbal/-/commits/master)
[![coverage report](https://gitlab.com/kisphp/dbal/badges/master/coverage.svg)](https://gitlab.com/kisphp/dbal/-/commits/master)

## Installation

Run in terminal

```sh
composer require kisphp/dbal
```

## Connect to database

```php
<?php

require_once 'path/to/vendor/autoload.php';

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Kisphp\Db\KisphpDbal;

$config = new Configuration();
$connectionParams = [
    'driver' => 'pdo_mysql', // or pdo_pgsql
    'host' => 'localhost',
    'dbname' => 'test',
    'user' => 'root',
    'password' => '',
    'charset'  => 'utf8',
    'driverOptions' => [
        1002 => 'SET NAMES utf8',
    ]
];

$connection = DriverManager::getConnection($connectionParams, $config);

$db = new KisphpDbal($connection);
```

## Database Insert

> `$db->insert('table_name', 'data array');`

```php
$db->insert('test_table', [
    'column_1' => 'value_1',
    'column_2' => 'value_2',
]);

// will return last_insert_id

```

## Database update

> `$db->update('table_name', 'data array', 'condition value', 'column name (default=id)');`

```php
$db->update('test_table', [
    'column_1' => 'value_1',
    'column_2' => 'value_2',
], [
    'id' => 1
]);

// will return affected_rows
```


## Get single value

```php
$value = $db->getValue("SELECT column_1 FROM test_table");
// or
$value = $db->getValue("SELECT column_1 FROM test_table WHERE id = :id", [ 'id' => 1 ]);
```

## Get pairs 

```php
$pairs = $db->getPairs("SELECT id, column_1 FROM test_table");

// or

$pairs = $db->getPairs("SELECT id, column_1 FROM test_table WHERE column_2 = :condition", [ 'condition' => 'demo' ]);

/*
will result
$pairs = [
     '1' => 'c1.1',
     '2' => 'c2.1',
     '3' => 'c3.1',
];
*/
```

## Get Custom query
 

```php
$query = $db->query("SELECT * FROM test_table");

// or

$query = $db->query("SELECT * FROM test_table WHERE column = :condition", [ 'condition' => 'demo' ]);

while ($item = $query->fetch(\PDO::FETCH_ASSOC)) {
    var_dump($item);
}
```
