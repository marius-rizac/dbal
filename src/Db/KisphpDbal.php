<?php

namespace Kisphp\Db;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

class KisphpDbal
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @param Connection $pdo
     */
    public function __construct(Connection $pdo)
    {
        $this->connection = $pdo;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * @param $table
     * @param array $data
     * @param array $types
     *
     * @return string
     * @throws \Doctrine\DBAL\Exception
     */
    public function insert($table, array $data, array $types = [])
    {
        $this->connection->insert($table, $data, $types);

        return $this->connection->lastInsertId();
    }

    /**
     * @param $table
     * @param array $data
     * @param array $criteria
     *
     * @return \Doctrine\DBAL\Result|int
     * @throws \Doctrine\DBAL\Exception
     */
    public function update($table, array $data, array $criteria, array $types = [])
    {
        return $this->connection->update($table, $data, $criteria, $types);
    }

    /**
     * @param string $query
     * @param array $conditions
     *
     * @return array
     */
    public function getPairs($query, array $conditions = [])
    {
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute($conditions);

        $output = [];
        while (($row = $result->fetchNumeric()) !== false) {
            $output[$row[0]] = $row[1];
        }

        return $output;
    }

    /**
     * @param string $query
     * @param array $conditions
     *
     * @return mixed
     */
    public function getValue($query, array $conditions = [])
    {
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute($conditions);

        $row = $result->fetchNumeric();

        return $row[0];
    }

    /**
     * @param string $query
     * @param array $conditions
     *
     * @return array
     */
    public function getRow($query, array $conditions = [])
    {
        /** @var Statement $stmt */
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute($conditions);

        return $result->fetchAssociative();
    }

    /**
     * @param $query
     * @param array $conditions
     *
     * @return \Doctrine\DBAL\Result
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function query($query, array $conditions = [])
    {
        $stmt = $this->connection->prepare($query);

        return $stmt->execute($conditions);
    }
}
